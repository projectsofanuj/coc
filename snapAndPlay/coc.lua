----- created by AnkuLua snap and play script ------
-----starting setting
immersive = true
setImmersiveMode(immersive)
genVersion = "8.3.0-PRO2"
setAutoGameArea = true
pcall(autoGameArea, setAutoGameArea)
scriptDimension = 1920
Settings:setScriptDimension(true, scriptDimension)
Settings:setCompareDimension(true, scriptDimension)
Settings:set("MinSimilarity", 0.86)





function imagelocation(imagearea,imagename,similarity,seconds)
    if imagearea==false then
        if (exists(Pattern(imagename):similar(similarity), seconds)) then
            return Location(getLastMatch():getX(), getLastMatch():getY())
        else            return false        end

    else
        if (imagearea:exists(Pattern(imagename):similar(similarity), seconds)) then
            return Location(imagearea:getLastMatch():getX(), imagearea:getLastMatch():getY())
        else            return false        end


    end
end

function clickimagelocation(y)
    if y then        click(y)        return true    else        return false    end
end

screen = getRealScreenSize()
width = screen:getX()
height = screen:getY()
toast(width .. "-" .. height)
compwidth= width
compheight = height

function resx(x)
    return math.floor((x / compwidth) * width)
end

function resy(y)
    return math.floor((y / compheight) * height)
end

function resRegion(x, y, w, h)
    ret = Region(resx(x), resy(y), resx(w), resy(h))
    return ret
end

function resLocation(x, y)
    return Location(resx(x), resy(y))
end

gold = 150000

elixPics = {    "elix8.png",    "elix9.png",    "elix10.png",    "elix11.png",    "elix12.png",    "elix13.png"}
checkelix = { ["elix8.png"] = "elix8full.png",              ["elix9.png"] = "elix9full.png",              ["elix10.png"] = "elix10full.png",              ["elix11.png"] = "elix11full.png",              ["elix12.png"] = "elix12full.png",              ["elix13.png"] = "elix13full.png"}

function waittime(t)
    return  os.time()-t
end




function matchcondition()
    goldcoinregion = resRegion(0, 100, 100, 500)
    if imagelocation(goldcoinregion,"goldcoinl.png",0.60,0) then
        goldvalue = numberOCRNoFindException(Region(goldcoinregion:getLastMatch():getX(), goldcoinregion:getLastMatch():getY() + resy(60), resx(400), resy(60)), "n")
        if (goldvalue >= gold) then return true else return false  end
    else        return false    end
end

function onattackbase()
    basesearchtime = os.time()
    while not imagelocation(resRegion(1521, 670, 1901 - 1521, 847 - 670),"next.png",0.70, 0) do
        timewaited = waittime(basesearchtime)
        if homebase() then
            startsearch()
        end
        if timewaited > 58 then return false end
    end
    return true
end

function deadbase()
    zoomoutattackbase()
    for i, t in ipairs(elixPics) do
        if (i == 1) then usePreviousSnap(false) else usePreviousSnap(true) end
        if imagelocation(false,t,0.60, 0) then
            if imagelocation(Region(getLastMatch():getX(), getLastMatch():getY(), resx(50), resy(50)),checkelix[t],0.9,0) then
                return true            else                return false            end
        end
    end
    return false
end

function homebase()
    if imagelocation(resRegion(8, 200, 140, 165) ,"message.png",0.80 ,5) then
        return true
    else return false end
end

function zoomoutattackbase()
    zoom(200, 600, 500, 400, 1450, 100, 600, 300, 5)
end

function troopstrained()
    slashreg = resRegion(200, 100, 200, 80)

    if (slashreg:exists(Pattern("ddot.png"):similar(0.60), 0)) then

        spx = slashreg:getLastMatch():getX()
        spy = slashreg:getLastMatch():getY()
        slashreg:exists(Pattern("ss.png"):similar(0.60), 0)
        mpx = slashreg:getLastMatch():getX()
        mpy = slashreg:getLastMatch():getY()
        trainedtroopsregion = Region(spx+4, spy - 10, mpx-spx, 65)

        trainedtroops = numberOCRNoFindException(trainedtroopsregion, "td")
        trainingtroopsregion = Region(mpx, mpy - 10, 300, 70)
        totaltroops = numberOCRNoFindException(trainingtroopsregion, "td")
        if (totaltroops > -1) then        return trainedtroops, totaltroops        else           return 0, 0        end

    else        return -2, -2    end

end

function troopstraining()
    if (exists(Pattern("armyman.png"):similar(0.50), 0)) then
        spx = getLastMatch():getX()
        spy = getLastMatch():getY()
        exists(Pattern("slash.png"):similar(0.50), 0)
        mpx = getLastMatch():getX()
        mpy = getLastMatch():getY()
        tar = Region(spx+4, spy - 5, mpx-spx, 65)
        local trainedarmy, ocr_result = numberOCRNoFindException(tar, "troop")
        adcr = Region(mpx+5, mpy-15, resx(420), resy(69))
        local armydoublecount, ocr_result = numberOCRNoFindException(adcr, "troop")
        if (trainedarmy > -1) then

            return trainedarmy, armydoublecount
        else            return 0, 0        end
    else        return -2,-2    end
end

function fulltrainarmy(armycapacity)
    giant = math.floor((armycapacity / 4) / 5)
    remarmcap = armycapacity - (giant * 5)
    arch = math.floor(remarmcap / 2)
    barb = remarmcap - arch
    continueClick(resx(307), resy(660), resx(140), resy(60), giant)
    continueClick(resx(104), resy(679), resx(140), resy(60), arch)
    continueClick(resx(107), resy(855), resx(140), resy(60), barb)
end

function errortohome()
    errorcheckstarttime = os.time()
    click(resLocation(1868,504))
    famsregion = resRegion(1765,13,1900-1765,142-13)
    if clickimagelocation(imagelocation(famsregion,"closefams.png",0.70,0)) then
        wait(1)
    end
    attac = resRegion(547, 300, 1291 - 547, 1050 - 300)
    if (attac:exists(Pattern("rh.png"):similar(0.60), 0)) then
        click(Location(attac:getLastMatch():getX(), attac:getLastMatch():getY()))
        wait(4)

    end

    while (homebase() == false) do
        if waittime(errorcheckstarttime)>100 then
            click(resLocation(1868,504))
            if clickimagelocation(imagelocation(resRegion(261,902,994-261,1073-902),"sclogin.png",0.60,10)) then

                wait(2)
                if currentaccount == 1 then
                    if height > 800 then
                        click(resLocation(1316, 219))
                    else
                        click(resLocation(1316, 364))
                    end

                else
                    if height > 800 then
                        click(resLocation(1316, 364))
                    else
                        click(resLocation(1316, 464))
                    end

                end
                wait(15)
            end
            errorcheckstarttime=os.time()
        end
    end
    click(resLocation(1868,504))
end

function armyready()
    --checking train troop icon
    if clickimagelocation(imagelocation(resRegion(0, 586, 310, 1069 - 586),"clicktotrain.png",0.40,1)) then
        wait(2)
    else
        return -2,-2
    end

    --checking trained troops
    trainedtroops, armycapacity = troopstrained()

    if trainedtroops == -2 then   return -2, -2    end

    --training troops
    click(resLocation(550, 56))
    troopsinqueue, armydblcap = troopstraining()
    if (troopsinqueue == 0) then
        fulltrainarmy(armycapacity)
        fulltrainarmy(armycapacity)
    elseif (troopsinqueue == armycapacity) then
        fulltrainarmy(armycapacity)




    elseif troopsinqueue~=armydblcap and armycapacity*2 ~= troopsinqueue then
        if 4>#tostring(troopsinqueue) and #tostring(troopsinqueue)>1 then
            if trainedtroops >armycapacity-5 then
                wait(40)
                continueClick(resLocation(1790,200),troopsinqueue-armycapacity)
                fulltrainarmy(armycapacity)

            end

        end


    end

    --brewing spells
    if clickimagelocation(imagelocation(false,"brewspells.png",0.70,0)) then
        continueMultiTouch({resLocation(218,669)},5)
    end
    --brewing siege machine
    if clickimagelocation(imagelocation(false,"buildsm.png",0.70,0)) then
        continueMultiTouch({resLocation(279,740,669)},2)
    end

    --close troops window
    if clickimagelocation(imagelocation(resRegion(1698, 0, 209, 112),"close.png",0.50,1)) then
    end
    return trainedtroops, armycapacity
end

function clickcordlist(x1, y1, x2, y2, n)
    points = {}
    for i = 1, n do
        a = i / n
        x = math.floor((1 - a) * x1 + a * x2)
        y = math.floor((1 - a) * y1 + a * y2)
        points[i] = Location(x, y)
    end
    return points
end

function trophyreduceattack()
    attackstarttime = os.time()
    zoomoutattackbase()
    swipe(resLocation(514, 231), resLocation(1530, 878))
    swipe(resLocation(512, 230), resLocation(1531, 888))

    troopselectionregion = resRegion(62, 867, 1778 - 62, 1069 - 867)
    trooploc = false
    for i, t in ipairs({"king.png","queen.png","warden.png","barb.png","arch.png","giant.png"}) do
        trooploc= imagelocation(troopselectionregion,t,0.60,0)
        if trooploc then
            break
        end
    end


    okaybuttonlocation = resLocation(1100,675)
    endbuttonregion = resRegion(0,722,256,867-722)
    endbuttonlocation = imagelocation(endbuttonregion,"endbattle.png",0.70,0)
    troopdroplocation = Location(304,515)
    if endbuttonlocation then
        locclick = {trooploc,troopdroplocation,trooploc,endbuttonlocation}
        continueMultiTouch(locclick,1)
        click(okaybuttonlocation)
    end
    --okaybuttonregion = resRegion(949,558,1394,807)
    --okaybuttonlocation = imagelocation(okaybuttonregion,"okay.png",0.70)


    attac = resRegion(547, 300, 1291 - 547, 1050 - 300)
     while true do
        if waittime(attackstarttime) > 190 then
            errortohome()
            return false
        end
         if(homebase()) then
             return true
         end
        if (attac:exists(Pattern("rh.png"):similar(0.60), 50)) then
            click(Location(attac:getLastMatch():getX(), attac:getLastMatch():getY()))
            break
        end
    end

    return true

end

troopscountregion = resRegion(62, 867, 1778 - 62, 1069 - 867)

function readycount(imagename)
    counttrailloc = imagelocation(troopscountregion,imagename,0.55,0)
    if not counttrailloc then
        return 0
    end
    cntr = resRegion(counttrailloc:getX(),counttrailloc:getY()-40,150,50)
    count = numberOCRNoFindException(cntr, "tt")
    return count
end


function attack(armycapacity)

    --going to upper part of screen
    swipe(resLocation(514, 231), resLocation(1530, 878))
    swipe(resLocation(512, 230), resLocation(1531, 888))


    attackstarttime = os.time()

    --troop image coords from select troop panel

    --getting number of troops


    giant = readycount("armygiant.png")
    arch = readycount("armyarch.png")
    barb = readycount("armybarb.png")

    if  armycapacity-6 > giant*5+arch+barb then
        giant = math.floor((armycapacity / 4) / 5)
        remarmcap = armycapacity - (giant * 5)
        arch = math.floor(remarmcap / 2)
        barb = remarmcap - arch


    end


    troopselectionregion = resRegion(62, 867, 1778 - 62, 1069 - 867)
    giantloc = imagelocation(troopselectionregion,"giant.png",0.60,0)
    barbloc = imagelocation(troopselectionregion,"barb.png",0.60,0)
    archloc = imagelocation(troopselectionregion,"arch.png",0.60,0)
    kingloc = imagelocation(troopselectionregion,"king.png",0.60,0)
    queenloc = imagelocation(troopselectionregion,"queen.png",0.60,0)
    wardenloc = imagelocation(troopselectionregion,"warden.png",0.60,0)
    machineloc = imagelocation(troopselectionregion,"machine.png",0.60,0)
    --lighteningspellloc = imagelocation(troopselectionregion,"lighteningspell.png",0.60,0)


--[[
    coordtable1 = {}
    n = 0
    allcords = clickcordlist(resx(341), resy(504), resx(1067), resy(43), halfofgiant)
    for _, v in ipairs(x) do
        n = n + 1;
        coordtable1[n] = v
    end

    for _, v in ipairs(y) do
        n = n + 1;
        z[n] = v
    end
--]]



    --troops to drop in single wave
    halfofgiant = math.floor(giant / 2)
    halfofarch = math.floor(arch / 2)
    halfofbarb = math.floor(barb / 2)

    -- getting coords for dropping troop wave 1
    giantcords1 = clickcordlist(resx(304), resy(515), resx(1067), resy(43), halfofgiant)
    giantcords1[#giantcords1+1] = barbloc
    barbcords1 = clickcordlist(resx(304), resy(515), resx(1067), resy(43), halfofbarb)
    barbcords1[#barbcords1+1] = archloc
    archcords1 = clickcordlist(resx(304), resy(515), resx(1067), resy(43), halfofarch)

    -- getting coords for dropping troop wave1
    giantcords2 = clickcordlist(resx(199), resy(335), resx(846), resy(858), halfofgiant+3)
    giantcords2[#giantcords2+1] = barbloc
    barbcords2 = clickcordlist(resx(199), resy(335), resx(846), resy(858), halfofbarb+15)
    barbcords2[#barbcords2+1] = archloc
    archcords2 = clickcordlist(resx(199), resy(335), resx(846), resy(858), halfofarch+15)


    --checking if not on homebase


    --dropping machine and select giant after that
    if machineloc then
        if #giantcords1 > 2 then
            machinedroploc = giantcords1[math.floor(#giantcords1/2)]
        else
            machinedroploc = resLocation(304,515)
        end
        continueMultiTouch({machineloc,machinedroploc,giantloc},1)
    else
        click(giantloc, 1)
    end

    --releasing first wave of troops
    continueMultiTouch(giantcords1, 1)
    continueMultiTouch(barbcords1, 1)
    continueMultiTouch(archcords1, 1)

    --going to lower part of screen
    swipe(resLocation(1438, 844), resLocation(155, 12))
    swipe(resLocation(1438, 844), resLocation(155, 12))

    --releasing second wave of troops
    click(giantloc, 1)
    continueMultiTouch(giantcords2, 1)
    continueMultiTouch(barbcords2, 1)
    continueMultiTouch(archcords2, 1)

    --releasing heroes
    if kingloc  then continueMultiTouch({kingloc,giantcords2[math.floor(#giantcords2/2)]},1) end
    if queenloc  then continueMultiTouch({queenloc,barbcords2[math.floor(#barbcords2/2)]},1) end
    if wardenloc  then continueMultiTouch({wardenloc,archcords2[math.floor(#archcords2/2)]},1) end
    wait(10)

    --boosting heroes
     if kingloc  then continueClick(kingloc, 1) end
    if queenloc  then continueClick(queenloc, 1) end
    if wardenloc  then continueClick(wardenloc, 1) end
    wait(20)


    -- wait loop for attack finish to go back to home
    returnhomeregion = resRegion(547, 300, 1291 - 547, 1050 - 300)

    while true do
        usePreviousSnap(false)
        if clickimagelocation(imagelocation(returnhomeregion, "rh.png", 0.60, 50)) then
            wait(8)
            if homebase() then
                return true
            end
        end
        if waittime(attackstarttime) > 205 then
            errortohome()
            return true
        end
    end

    return true
end

function startsearch()
    while true do
        while true do
            click(resLocation(261,895))
            if (exists(Pattern("startattackb.png"):similar(0.70), 0)) then
                click(Location(getLastMatch():getX(), getLastMatch():getY()))
                break
            else
                if (exists(Pattern("startattacka.png"):similar(0.70), 0)) then
                    click(Location(getLastMatch():getX(), getLastMatch():getY()))
                    break
                else
                    errortohome()

                end
            end
        end

        wait(2)
        if (exists(Pattern("fams.png"):similar(0.70), 0)) then
            click(Location(getLastMatch():getX()+15, getLastMatch():getY()+15))
            break
        else
            errortohome()
        end
    end

end

--[[
function checkerroringame()
    if (exists(Pattern("tryagain.png"):similar(0.80), 0)) then
        click(resLocation(10, 10))
        return true
    else
        return false
    end
end
--]]

function checkbasestate()
    local val, ocr_result = numberOCRNoFindException(resRegion(1549, 26, 306, 69), "tt")

end

function collecthomebaseresources()
    clr = resRegion(490,143,1675-490,935-143)
    if clickimagelocation(imagelocation(clr,"homebasedark.png",0.80,0)) then

    end
    if clickimagelocation(imagelocation(clr,"homebasegold.png",0.80,0)) then

    end
    if clickimagelocation(imagelocation(clr,"homebaseelix.png",0.80,0)) then

    end

end

function switchaccounts(account)
    settingbuttonregion = resRegion(1733,712,1905-1733,855-712)
    if clickimagelocation(imagelocation(settingbuttonregion,"setting.png",0.60,0)) then

        wait(1)
    end
    disconnectregion = resRegion(1189,110,1330-1189,239-110)

    if clickimagelocation(imagelocation(disconnectregion,"disconnected.png",0.60,0)) then

        wait(10)
    end
    if account==1 then
        if height>800 then
            click(resLocation(1316,219))
        else
            click(resLocation(1316,364))
        end

    else
        if height>800 then
            click(resLocation(1316,364))
        else
            click(resLocation(1316,464))
        end

    end

    waithomebase = os.time()
    while not homebase() do
        if waittime(waithomebase)>60 then
            errortohome()
        end

    end

    return true
end
toast("CLASH OF CLANS BOT - PINKU BOTS ")
wait(2)
deviceimei = getIMEI()
purchased = httpGet("http://pinkubots.pythonanywhere.com/goodbot/"..deviceimei.."/1")

if purchased ~= "true" then
    dialogInit()
    addTextView("CLASH OF CLANS BOT - PINKU BOTS ")
    newRow()
    addTextView("You Have Not Purchased Bot If Purchased Recently Wait For 5-30 Minutes While Server Activates It")
    newRow()
    addTextView("http://pinkubots.pythonanywhere.com")
    newRow()
    dialogShow("Purchase Verification Failed")
    scriptExit()

end


-- Game is starting  ui
toast("CLASH OF CLANS BOT - PINKU BOTS ")
dialogInit()
addTextView("CLASH OF CLANS BOT - PINKU BOTS ")
newRow()
addTextView("First Account")
newRow()
addTextView("SET ARMY CAMP SIZE FOR FIRST ACCOUNT")
addEditText("firstarmyvalue", "205")
newRow()
addCheckBox("secondaccount", "Second Account ", false)
newRow()
addTextView("SET ARMY CAMP SIZE FOR SECOND ACCOUNT")
addEditText("secondarmyvalue", "280")
newRow()
addTextView("Select Currently Active Account")
newRow()
addCheckBox("ca1", "1", false)
newRow()
addCheckBox("ca2", "2", false)
newRow()
addTextView("Select Only One Option From Tropy Reducer And Dead Base Attack")
newRow()
addCheckBox("cbValue", "Trophy Reducer", false)
newRow()
addCheckBox("ccbValue", "Dead Base Attack", false)
dialogShow("BOT OPTIONS")

firstarmycapacity = firstarmyvalue
secondarmycapacity = secondarmyvalue

currentaccount=0
if ca1 then
    currentaccount = 1
else
    currentaccount = 2
end



while true do

    click(resLocation(1868,504))
    collecthomebaseresources()
    if (cbValue) then
        toast("Skipping Army Training - Trophy Reducer Mode")
    else
        armyreaady = 0
        while (armyreaady == 0) do
            trainedtroops, uarmycapacity = armyready()
            if trainedtroops == -2 then
                errortohome()
            else
                armyreaady = 1
            end
        end
        while (armycapacity-6>trainedtroops ) do
            if trainedtroops == -2 then
                errortohome()
                trainedtroops, uarmycapacity = armyready()
            end
            tempwaittime = armycapacity - trainedtroops
            if tempwaittime > 80 then
                if secondaccount then
                    if currentaccount==1 then
                        switchaccounts(2)
                        armycapacity = secondarmycapacity
                        currentaccount = 2
                    else
                        switchaccounts(1)
                        armycapacity = firstarmycapacity
                        currentaccount = 1
                    end

                end

            else
                wait(tempwaittime)
                end

            trainedtroops, uarmycapacity = armyready()

        end
        wait(20)

    end
    startsearch()
    while true do
        searchstarttime = os.time()
        while (onattackbase() == false) do
            if waittime(searchstarttime) > 40 then
                errortohome()
                startsearch()
                searchstarttime = os.time()

            end

        end

        if (cbValue) then
            trophyreduceattack()
            break
        else
            --if (matchcondition()) then
            if (deadbase()) then
                if (attack(armycapacity)) then
                    break
                else
                    errortohome()
                    break
                end

                --end
            else
                click(resLocation(1697, 744))
                usePreviousSnap(false)
                wait(2)
            end
            --]]
        end
    end
    waitforhomebase = os.time()
    while (homebase() == false) do
        if waittime(waitforhomebase)>60 then
            errortohome()
        end
    end
    if (cbValue) then
        toast("Going For Next Attack")
    else
        --checkbasestate()
        wait(1)
    end

end



--[[commented stuff
on starting of loop
 --zoomoutattackbase()
    --swipe(Location(514, 231), Location(1530, 878))
halfofbarb

--]]
